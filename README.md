﻿# SourceFileCombiner

A file combiner to support multiline C# solutions for Codingame problems.

Given multiple files, all distinct imports will be hoisted to the top of the output file, everything else will be put into separate regions named after the source filename.

## To package

```bash
SourceFileCombiner\> dotnet tool install -g dotnet-warp
SourceFileCombiner\> dotnet-warp
```

## To run

```bash
SourceFileCombiner\> .\SourceFileCombiner.exe
    --input-path SampleInput\
    --file-pattern *.cs
    --output-path SampleOutput\result.cs
```

## To watch

```bash
SourceFileCombiner\> .\SourceFileCombiner.exe
    --watch
    --input-path SampleInput\
    --file-pattern *.cs
    --output-path SampleOutput\result.cs
```

---

## MIT License

Copyright (c) [2019] [tugend@gmail.com]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
