﻿using CommandLine;
using System;
using System.Collections.Generic;

namespace SourceFileCombinerTests
{
    public class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                   .WithParsed(o =>
                   {
                       Console.WriteLine("Combining files from : {0}", o.InputDirectoryPath);
                       Console.WriteLine("    with pattern     : {0}", o.InputFileNamePattern);
                       Console.WriteLine("    to output        : {0}.", o.OutputFilePath);

                       void run() => SourceFileCombiner.Combiner.Combine(o.InputDirectoryPath, o.InputFileNamePattern, o.OutputFilePath);

                       if (o.Watch) {
                           Watcher.Watch(o.InputDirectoryPath,  o.InputFileNamePattern,  new List<string>() { @"\.TMP$" }, run);
                       }
                       else 
                       {
                           run();
                       }
                   });
        }

        private class Options
        {
            [Option('w', "watch", Required = false, Default = false, HelpText = "Retranspile on file changes.")]
            public bool Watch { get; set; }

            [Option('i', "input-path", Required = true, HelpText = "The directory to transpile from")]
            public string InputDirectoryPath { get; set; }

            [Option('p', "file-pattern", Required = true, HelpText = "The pattern defining which files to include")]
            public string InputFileNamePattern { get; set; }

            [Option('o', "output-path", Required = true, HelpText = "The path for the output file")]
            public string OutputFilePath { get; set; }
        }
    }
}
