﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using static System.Environment;
using System.Collections.Generic;

namespace SourceFileCombiner
{
    /// <summary>
    /// ✓: filepatterns for cli logging
    /// ✓: structured combination of import statements 
    /// ✓: introduce regions corresponding to the source files
    /// ✓: write some tests
    /// ✓: add cli wrapper with help options
    /// TODO: add a watch and compile to an executable 
    /// </summary>
    public class Combiner
    {
        public static void Combine(string inputDirectoryPath, string inputFileNamePattern, string outputFilePath)
        {
            var inputFilePaths = Directory.GetFiles(inputDirectoryPath, inputFileNamePattern);
            
            var (imports, files) = inputFilePaths
               .Select(ParseFile)
               .Aggregate(
                   (
                       imports: new List<string>(),
                       files: new List<(string path, List<string> content)>()
                   ),
                   (accum, next) =>
                   (
                       imports: accum.imports
                           .Concat(next.imports)
                           .ToList(),
                       files: accum.files
                           .Append((next.path, next.code))
                           .ToList()
                   ));

            using (var outputStream = File.Create(outputFilePath))
            {
                if (imports.Any()) {
                    List<string> importBlock = imports
                        .Distinct()
                        .Append(NewLine)
                        .ToList();

                    outputStream.WriteUtf8(importBlock); 
                }

                for (var i = 0; i < files.Count; i++)
                {
                    var (path, content) = files[i];

                    var startRegion = "#region " + Path.GetFileName(path) + NewLine;
                    var endRegion = NewLine + "#endregion";
                    var regionDivider = NewLine + NewLine;

                    outputStream.WriteUtf8(startRegion);
                    outputStream.WriteUtf8(content);
                    outputStream.WriteUtf8(endRegion);
                    if (i < inputFilePaths.Length - 1) { outputStream.WriteUtf8(regionDivider); }
                }
            }
        }

        private static (string path, List<string> imports, List<string> code) ParseFile(string path)
        {
            var content = File.ReadAllLines(path);

            var importsAndWhitespace = content
                .TakeWhile(line => line.StartsWith("using") || string.IsNullOrWhiteSpace(line))
                .ToList();

            var imports = importsAndWhitespace
                .Where(line => !string.IsNullOrWhiteSpace(line))
                .ToList();

            var code = content
                .Skip(importsAndWhitespace.Count)
                .ToList();

            return (path, imports, code);
        }
    }

    static class FileStreamEx
    {
        public static void WriteUtf8(this FileStream outputStream, string text)
        {
            outputStream.Write(Encoding.UTF8.GetBytes(text));
        }

        public static void WriteUtf8(this FileStream outputStream, List<string> lines)
        {
            outputStream.WriteUtf8(string.Join(NewLine, lines));
        }
    }
}
