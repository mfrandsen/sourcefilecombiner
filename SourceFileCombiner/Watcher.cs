﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SourceFileCombinerTests
{
    public class Watcher
    {
        public static void Watch(string path, string filter, List<string> ignorePatterns, Action onChange) => 
            new Watcher(path, filter, ignorePatterns, onChange).Watch();

        private readonly List<string> ignorePatterns;
        private readonly Action onChange;
        private readonly string path;
        private readonly string filter;

        private long changeNumber = 0;
        private bool pending = false;
        private DateTime lastUpdate = DateTime.UtcNow;

        private Watcher(string path, string filter, List<string> ignorePatterns, Action onChange)
        {
            this.path = path;
            this.filter = filter;
            this.ignorePatterns = ignorePatterns;
            this.onChange = onChange;
        }

        private void Watch()
        {
            using (FileSystemWatcher watcher = new FileSystemWatcher(path, filter))
            {
                // Add event handlers.
                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;
                watcher.Deleted += OnChanged;
                watcher.Renamed += OnChanged;

                watcher.IncludeSubdirectories = true;
                watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName;

                watcher.EnableRaisingEvents = true;

                Console.WriteLine();
                Console.WriteLine("Press 'q' to quit.");
                while (Console.Read() != 'q') ;
            }
        }

        private async void OnChanged(object source, FileSystemEventArgs e)
        {
            if (ignorePatterns.Any(pattern => Regex.IsMatch(e.FullPath, pattern))) return;
            if (pending) return;
            if (lastUpdate.AddMilliseconds(100) >= DateTime.UtcNow) return;

            if (lastUpdate.AddSeconds(2) >= DateTime.UtcNow)
            {
                pending = true;
                await Task.Run(() => Thread.Sleep(2000));
            }

            pending = true;
            lastUpdate = DateTime.UtcNow;

            var maxTries = 0;
            while(maxTries++ < 30) { 
                try
                {
                    onChange();
                    Console.WriteLine($"File: {e.FullPath} {e.ChangeType} ({changeNumber++})");
                    break;
                }
                catch
                {
                    Console.WriteLine(".");
                    Thread.Sleep(10);
                }
            }
            pending = false;
        }
    }
}
