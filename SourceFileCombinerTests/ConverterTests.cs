﻿using Xunit;
using Shouldly;
using System.IO;
using SourceFileCombiner;

public class ConverterTests
{
    private static readonly string inputDirectory = Path.Combine(Directory.GetCurrentDirectory(), "..", "..", "..", "input");
    private static readonly string tempDirectory = Path.Combine(Directory.GetCurrentDirectory(), "..", "..", "..", "temp");
    private static readonly string outputTarget = Path.Combine(tempDirectory, "result.cs");

    public ConverterTests()
    {
        if (Directory.Exists(tempDirectory)) { 
            Directory.Delete(tempDirectory, true);
        }
     
        Directory.CreateDirectory(tempDirectory);
    }

    [Fact]
    public void Sanity()
    {
        Directory.Exists(tempDirectory).ShouldBeTrue("temp directory should exist");
        Directory.Exists(inputDirectory).ShouldBeTrue("input directory should exist");
    }

    [Fact]
    public void Rejects_bad_path()
    {
        Assert.Throws<DirectoryNotFoundException>(() => Combiner.Combine("bad/path", "*.cs", tempDirectory));
    }

    [Fact]
    public void Outputs_same_given_single_file()
    {
        Combiner.Combine(inputDirectory, "BaseCase/SomeClass_1.cs", outputTarget);

        var result = File.ReadAllText(outputTarget);

        result.ShouldBe(
@"#region SomeClass_1.cs
public class SomeClass_1 { }
#endregion"
        );
}

[Fact]
    public void Outputs_combined_file_given_multiple_files()
    {
        Combiner.Combine(inputDirectory, "BaseCase/SomeClass_*.cs", outputTarget);

        var result = File.ReadAllText(outputTarget);

        result.ShouldBe(
@"#region SomeClass_1.cs
public class SomeClass_1 { }
#endregion

#region SomeClass_2.cs
public class SomeClass_2 { }
#endregion"
        );
    }

    [Fact]
    public void Includes_imports_for_single_file()
    {
        Combiner.Combine(inputDirectory, "SimpleImports/SomeClassWithImports_1.cs", outputTarget);

        var result = File.ReadAllText(outputTarget);

        result.ShouldBe(
@"using System;

#region SomeClassWithImports_1.cs
public class SomeClassWithImports_1 
{
    public void Hello() { Console.WriteLine(""hello world""); }
}
#endregion"
        );
    }

    [Fact]
    public void Combines_imports_for_multiple_files()
    {
        Combiner.Combine(inputDirectory, "SimpleImports/SomeClassWithImports_*.cs", outputTarget);

        var result = File.ReadAllText(outputTarget);

        result.ShouldBe(
@"using System;
using System.IO;

#region SomeClassWithImports_1.cs
public class SomeClassWithImports_1 
{
    public void Hello() { Console.WriteLine(""hello world""); }
}
#endregion

#region SomeClassWithImports_2.cs
public class SomeClassWithImports_2
{
    public void Hello() { Directory.Exists(""something that requires using System.IO""); }
}
#endregion"
        );
}

[Fact]
    public void Repeated_imports_are_pruned()
    {
        Combiner.Combine(inputDirectory, "DuplicateImports/SomeClassWithImports*.cs", outputTarget);

        var result = File.ReadAllText(outputTarget);

        result.ShouldStartWith(
@"using System;

#region"
        );
    }
}
